import React, {Component, useContext, useRef, useEffect } from 'react';
import contactContext from "../../context/contact/contactContext";

const ContactFilter = () => {
  const ContactContext = useContext(contactContext);
  const text = useRef('');

  const { filtered } = ContactContext;

  useEffect(() => {
    if(filtered === null){
     text.current.value = '';
    }
  });


  const onChange = e => {
    if(text.current.value !== ''){
      ContactContext.filterContacts(e.target.value);
    } else {
      ContactContext.clearFilter();
    }
  };


    return (
      <form>
        <input ref={text} type="text" placeholder="Filter Contacts..." onChange={onChange} />
      </form>
    );
};

export default ContactFilter;