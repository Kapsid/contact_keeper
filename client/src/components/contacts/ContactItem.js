import React, {Component, useContext} from 'react';
import contactContext from "../../context/contact/contactContext";

const ContactItem = ({ contact }) => {

    const ContactContext = useContext(contactContext);
    const { name, _id, email, phone, type} = contact;
    const { deleteContact, setCurrent, clearCurrent } = ContactContext;
    const onDelete = () => {
      deleteContact(_id);
      clearCurrent();
    };
    return (
      <div className='card bg-light'>
        <h3 className='text-primary text-left'>
          {name}{' '}
          <span style={{ float: 'right' }} className={'badge ' + (type === 'professional' ? 'badge-success' : 'badge-primary')} >
            {type.charAt(0).toUpperCase() + type.slice(1)}
          </span>
        </h3>
        <ul className='list'>
          {email && (<li>
            <i className='fas fa-envelope-open' /> {email}
          </li>)}
          {phone && (<li>
            <i className='fas fa-phone' /> {phone}
          </li>)}
        </ul>
        <p>
          <button className='btn btn-dark' onClick={() => setCurrent(contact)}>Edit</button>
          <button className='btn btn-danger' onClick={onDelete}>Delete</button>
        </p>
     </div>
    );
};

export default ContactItem;