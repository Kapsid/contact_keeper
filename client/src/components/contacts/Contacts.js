import React, {Fragment, useContext, useEffect} from 'react';
import contactContext from "../../context/contact/contactContext";
import {CSSTransition, TransitionGroup} from "react-transition-group";
import ContactItem from "./ContactItem";
import Spinner from "../layout/Spinner";

const Contacts = () => {

    const ContactContext = useContext(contactContext);

    const { contacts, filtered, getContacts, loading } = ContactContext;

    useEffect(() => {
      getContacts();
    });

    if(contacts !== null && contacts.length === 0 && !loading){
      return <h4>Please add a contact</h4>;
    }

    return (
      <Fragment>
        {contacts !== null && !loading ? <TransitionGroup>
          {filtered !== null ? filtered.map(contact =>
              (<CSSTransition key={contact.id} timeout={500} classNames='item'>
                <ContactItem contact={contact} />
              </CSSTransition>)) :
            contacts.map(contact =>
              <CSSTransition key={contact.id} timeout={500} classNames='item'>
                <ContactItem contact={contact} />
              </CSSTransition>
            )}
        </TransitionGroup> : <Spinner />}

      </Fragment>
    );
};

export default Contacts;