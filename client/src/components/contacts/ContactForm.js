import React, { useState, useContext, useEffect } from 'react';
import contactContext from "../../context/contact/contactContext";

const ContactForm = () => {

    const ContactContext = useContext(contactContext);

    const { addContact, current, clearCurrent, updateContact } = ContactContext;

    useEffect(() => {
      if(current != null){
        setContact(current);
      } else {
        setContact({
          name: '',
          email: '',
          phone: '',
          type: 'personal'
        });
      }
    }, [contactContext,current]);

    const [contact, setContact] = useState({
      name: '',
      email: '',
      phone: '',
      type: 'personal'
    });

    const onChange = e => setContact({...contact, [e.target.name]: e.target.value });

    const onSubmit = e => {
      if(current === null){
        addContact(contact);
      }
      else{
        updateContact(contact);
      }

      setContact({
        name: '',
        email: '',
        phone: '',
        type: 'personal'
      });
    };

    const clearAll = () => {
      clearCurrent();
    };

    const { name, email, phone, type} = contact;
    return (
      <form onSubmit={onSubmit}>
        <h2 className='text-primary'>{current ? 'Edit Contact' : 'Add Contact'}</h2>
        <input type='text' placeholder='name' name='name' value={name} onChange={onChange}/>
        <input type='email' placeholder='E-mail' name='email' value={email} onChange={onChange}/>
        <input type='phone' placeholder='Phone' name='phone' value={phone} onChange={onChange}/>
        <h5>Contact Type</h5>
        <input type='radio' name='type' value='personal' checked={type === 'personal'} onChange={onChange}/> Personal{' '}
        <input type='radio' name='type' value='professional' checked={type === 'professional'} onChange={onChange}/> Professional
        <div>
          <input type='submit' value={current ? 'Update Contact' : 'Add Contact'} className='btn btn-primary btn-block' />
        </div>
        {current &&
          <div>
            <button className='btn btn-block btn-light' onClick={clearAll}>Clear</button>
          </div>
        }
      </form>
    );
};

export default ContactForm;